export interface ToDoItem {
    title: string;
    done?: boolean;
}
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ToDoItem } from 'src/app/interfaces/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent {
  
  inputText: string = '';
  list: ToDoItem[] = [
    { title: 'Покормить кота', done: false },
    { title: 'Купить хлеб', done: true }
  ]
  editingIndex: number = -1;

  check(idx: number): void {
     this.list = this.list.map(
         (item: ToDoItem, i: number) => idx === i 
             ? { ...item, done: !item.done } 
             : item
     );
  }
  addItem(): void {
    if (!this.inputText) {
      return;
    }
    const title: string = this.inputText;
    const newItem = { title, done: false };

    this.list.push(newItem);
    this.inputText = '';
  }
  remove(idx: number): void {
    this.list = this.list.filter((item: ToDoItem, i: number) => idx !== i);
  }

  edit(idx: number): void {
    this.inputText = this.list[idx].title;
    this.editingIndex = idx;
  }

  editItem(): void {
    const title: string = this.inputText;
    this.list[this.editingIndex].title = title;
    this.list[this.editingIndex].done = false;    
    this.inputText = '';
    this.editingIndex = -1;
  }
  clearEdit(): void {
    this.inputText = '';
    this.editingIndex = -1;
  }

  isEditing(): boolean {
    return this.editingIndex !== -1 ? true : false;
  }

}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToDoItem } from 'src/app/interfaces/todo';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {

  @Input() index: number;
  @Input() item: ToDoItem;
  @Input() editingIndex: number;
  @Output() onRemove: EventEmitter<number> = new EventEmitter<number>();
  @Output() onEdit: EventEmitter<number> = new EventEmitter<number>();
  @Output('check') onCheck: EventEmitter<void> = new EventEmitter<void>();

  constructor() {
    this.item = {title: ''};
    this.index = 0;
    this.editingIndex = -1;
  }

  remove(): void {
    this.onRemove.emit(
      this.index
    )
  }

  edit(): void {
    this.onEdit.emit(
      this.index      
    )
  }

  showActive(idx: number): boolean {
    return idx === this.index ? true : false;
  };  
}